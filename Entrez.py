from collections import OrderedDict

from Bio import Entrez

Entrez.email = "marcomunozperez@uma.es"  # NCBI will email you in case query limit being reached


def genes_in_organism(term):
    handle_search = Entrez.esearch(db="gene", term=term, retmax=10)
    records = Entrez.read(handle_search)
    summary_records = OrderedDict()
    gene_pos = dict()
    for record in records['IdList']:
        handle_summary = Entrez.esummary(db="gene", id=record)
        summary_records[record] = Entrez.read(handle_summary)
        if summary_records[record]['DocumentSummarySet']['DocumentSummary'][0]['Name'] != 'NEWENTRY':
            gene_pos[record] = [
                summary_records[record]['DocumentSummarySet']['DocumentSummary'][0]['GenomicInfo'][0]['ChrStart'],
                summary_records[record]['DocumentSummarySet']['DocumentSummary'][0]['GenomicInfo'][0]['ChrStop']]
        handle_summary.close()
    handle_search.close()
    return gene_pos

