from Bio import Phylo, SeqIO
import pandas as pd
from Entrez import *
import random


def colors(n):
    """
    Generates n visually distinct RGB colors
    :param n: number of colors to generate
    :return: list of rgb's (r, g, b)
    """
    ret = []
    r = int(random.random() * 256)
    g = int(random.random() * 256)
    b = int(random.random() * 256)
    step = 256 / n
    for i in range(n):
        r += step
        g += step
        b += step
        r = int(r) % 256
        g = int(g) % 256
        b = int(b) % 256
        ret.append((r, g, b))
    return ret


class ClusterPlot:
    """
    Class for generating phylogenetic trees in ascii and Newick format
    """

    def __init__(self, file_path, file_type='newick'):
        self.file_path = file_path
        self.file_type = file_type

    def draw_newick(self):
        tree = Phylo.read(self.file_path, self.file_type)
        tree.ladderize()
        Phylo.draw(tree)

    def draw_ascii(self, dest_file_path):
        tree = Phylo.read(self.file_path, self.file_type)
        with open(dest_file_path, 'w') as dest_file:
            Phylo.draw_ascii(tree, file=dest_file)


def get_biggest_line_ascii(ascii_file_path):
    with open(ascii_file_path, 'r') as tree_file:
        biggest = 0
        for line in tree_file:
            if len(line) > biggest:
                biggest = len(line)
    return biggest


def add_msa_to_tree_ascii(tree_file_path, msa_file_path, dest_file_path):
    """
    Appends sequences to an ascii dendogram
    :param tree_file_path: path to the ascii tree file
    :param msa_file_path: path to the msa fasta file
    :param dest_file_path: path to the resulting file
    """
    dest_file = open(dest_file_path, 'w')
    msa_file = open(msa_file_path, 'r')
    records = list(SeqIO.parse(msa_file_path, "fasta"))
    line_size = get_biggest_line_ascii(tree_file_path)  # For properly aligning all sequences
    with open(tree_file_path, 'r') as tree_file:
        for line in tree_file:
            if len(line.split()) > 1:
                line = line.strip('\n')
                header = line.split()[1].strip('...')
                for seq in records:
                    if header == seq.id:  # If tree leaf name matches the sequence id
                        dest_file.write(
                            line + (' ' * (line_size - len(line))) +
                            '=' +
                            str(seq.seq) + '\n')
            else:
                dest_file.write(line)
    dest_file.close()


def create_table_from_df(datf, table_dest, gene_regions):
    """
    Creates an HTML file with a table representing a msa with a dendogram to its left
    and gene regions at the bottom.
    :param datf: pandas df with tree leafs as index values and a sequence as row elements
    :param table_dest: file path to the resulting generated HTML file
    :param gene_regions: dictionary with gene id as keys and start-stop positions as a list
    """
    file_out = open(table_dest, 'w')
    column_values = datf.columns.values.tolist()
    column_values_tagged = list()
    for value in column_values:  # create a list with html formated table headers
        value = str(value)
        if '...' in value:
            value = '...'
        column_values_tagged.append('<th> ' + value + ' </th>\n')
    # Define colors for each nucleotide
    nuc_color = {'A': 'red', 'C': 'blue', 'G': 'green', 'T': 'yellow'}
    # Write basic structure to file
    file_out.write('<html>\n')
    file_out.write("""
    <head>
    <style>
    table, th, td {
        border: 1px solid black;
        border-collapse: collapse;
        white-space: pre;
    }
    </style>
    </head>
    """)
    file_out.write('<body>\n')
    file_out.write('<table>\n')
    file_out.write('<tr>\n')
    file_out.write('<th> ID </th>\n')
    for col in column_values_tagged:  # Write formated table headers to file
        file_out.write(col)
    file_out.write('</tr>\n')

    for row in datf.iterrows():  # For each row in df add a row to the HTML table
        index, data = row
        file_out.write('<tr>\n')
        if (not any(elem is None for elem in data)) and index.strip() not in ['|', '||']:
            file_out.write('<td> ' + index + '</td>\n')
            for nuc in data:
                file_out.write('<td bgcolor="' + nuc_color.get(nuc, "black") + '"> ' + nuc + '</td>\n')
        elif index.strip() not in ['|', '||']:
            file_out.write('<td> ' + index + '</td>\n')
        file_out.write('</tr>\n')

    #  Add gene information to the bottom
    file_out.write('<tr><td align="center"><b> ' + 'GENE REGIONS' + '</b></td></tr>\n')

    gene_colors = colors(len(gene_regions))
    for gi, gene in enumerate(gene_regions):  # For each gene in the dict, add gene id
        i_pos = min(gene_regions[gene])
        e_pos = max(gene_regions[gene])
        if (int(gene_regions[gene][1]) - int(gene_regions[gene][0])) > 0:
            direction = '>'
        else:
            direction = '<'
        file_out.write('<tr>\n')
        file_out.write('<td style="background-color:rgb' + str(gene_colors[gi]) +
                       ';" align="center"> ' + gene + '</td>\n')

        for index, value in enumerate(column_values):  # Colour table cells positions that are within the gene
            match = ' '
            file_out.write('<td ')
            # If position within the gene or '...' found, colour the cell
            if (str.isdigit(str(value)) and int(i_pos) <= value <= int(e_pos)) \
                    or ('...' in str(value) and int(i_pos) <= last_value <= int(e_pos) and int(i_pos) <= column_values[
                index + 1] <= int(e_pos)):
                if value in [int(i_pos), int(e_pos)]:  # If position matches with gene start/end pos
                    match = '*'
                else:
                    match = direction
                file_out.write('style="background-color:rgb' + str(gene_colors[gi]) + ';"')
            file_out.write(' align="center">' + match + '</td>\n')
            last_value = value

        file_out.write('</tr>\n')

    file_out.write('</table>\n')
    file_out.write('</body>\n')
    file_out.write('</html>\n')
    file_out.close()


def ascii_table_to_df(ascii_file_path, include_non_mutating=False):
    """
    Generates a pandas df from an ascii tree + msa file
    :param include_non_mutating:
    :param ascii_file_path: ascii file path
    :return: pandas df
    """
    with open(ascii_file_path, 'r') as asciin:
        index = list()
        values = list()
        for line in asciin:
            if len(line.split()) == 3:
                index.append(line.split('=')[0])  # "=" used as separator between tree leaf and sequence
                nuc_list = list()
                for nuc in line.split('=')[1].strip('\n'):
                    nuc_list.append(nuc)
                values.append(nuc_list)
            else:
                index.append(line.strip('\n'))
                values.append([])
        df = pd.DataFrame(values, index=index)
        if not include_non_mutating:
            for col in list(df.columns.values):  # Drops columns where all nucleotides are the same
                column = df[col].tolist()
                column = [x for x in column if x is not None]
                if all(x == column[0] for x in column):
                    df = df.drop([col], axis=1)
            last_col = list(df.columns.values)[0]
            for idx, col in enumerate(list(df.columns.values)):  # Adds columns to indicate that there is a gap
                if col - last_col > 1:
                    new_col = list(' ' * len(df.index))
                    df.insert(loc=list(df.columns.values).index(last_col) + 1,
                              column=str(last_col + 1) + '...' + str(col - 1),
                              value=new_col)
                last_col = col

    return df

dest_path = 'Virus/treeAndMsa.txt'
html_file = 'Virus/table.html'
create_table_from_df(ascii_table_to_df(dest_path, include_non_mutating=True), html_file, genes_in_organism('txid219744'))