import plotly.offline as pof
import plotly.figure_factory as ff
import plotly.graph_objs as go


def create_table(data_matrix):
    to_list = data_matrix.as_matrix()
    new_list = list()
    for n, i in enumerate(to_list):
        new_list.append(list(list(data_matrix.index.values)[n]) + list(i))

    new_list = [['x']+list(data_matrix.columns.values)] + new_list
    table = ff.create_table(new_list, index=True)
    pof.iplot(table)


def plot_gene_mutations(gene_mutation_dict):
    labels = list(gene_mutation_dict.keys())
    values = list(gene_mutation_dict.values())
    trace = go.Pie(labels=labels, values=values)
    pof.iplot(
        {'data': [trace],
         'layout': {'title': 'Mutations distribution amongst genes'}})
