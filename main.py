import MuscleMSA
import TreePlot
from Utils import get_mutations_in_msa
from Plot import *
# In[1]: Define file names
fasta_file = 'virus/filtered_haplotypes.fasta'
aligned_sequence_file = 'virus/filtered_haplotypes.afa'
tree_file = 'virus/filtered_haplotypes_tree.txt'
tree_ascii_file = 'virus/filtered_haplotypes_ascii_tree.txt'

# In[2]: Do a MSA and get a tree from MSA
print('Creating MSA')
MuscleMSA.Muscle.msa(fasta_file, aligned_sequence_file)
print('Making tree')
MuscleMSA.Muscle.make_tree(aligned_sequence_file, tree_file)
cp = TreePlot.ClusterPlot(tree_file)
cp.draw_ascii(tree_ascii_file)

# In[3]: Get mutation evolution from aligned FASTA file
file_out = 'virus/filtered_haplotypes_mutation_evolution.txt'
print('Getting mutations')
gene_mutations, mutation_table = get_mutations_in_msa(aligned_sequence_file, file_out, gene_orgn='txid219744')
print(mutation_table)
create_table(mutation_table)