import subprocess


class Muscle:
    @staticmethod
    def msa(fasta_input, filepath_out):
        subprocess.Popen(
            ['./muscle', '-in', fasta_input, '-out', filepath_out]
        ).wait()

    @staticmethod
    def make_tree(aligned_fasta_input, filepath_out):
        command = ['./muscle', '-maketree', '-in', aligned_fasta_input, '-out', filepath_out]
        subprocess.Popen(command).wait()
