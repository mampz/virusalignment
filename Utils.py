from collections import OrderedDict

from Bio import SeqIO
from tqdm import tqdm
import Entrez
import pandas as pd


def get_mutations(sequence1, sequence2, result_file=None, gene_pos=None, gene_mutations=None):
    sequence1_seq = str(sequence1.seq)
    sequence2_seq = str(sequence2.seq)
    mutations = list()
    if result_file is None:
        print('Comparing ' + sequence1.id + ' and ' + sequence2.id)
        for N in range(len(sequence1_seq)):
            if sequence1_seq[N] != sequence2_seq[N]:
                mutations.append(N)
                if gene_pos is None:
                    print('Position: ' + str(N) +
                          ' Sequence1: ' + sequence1_seq[N] +
                          ' Sequence2: ' + sequence2_seq[N])
                else:
                    nucleotide_in_gene_res = nucleotide_in_gene(N, gene_pos, gene_mutations)
                    gene_mutations = nucleotide_in_gene_res[1]
                    print('Position: ' + str(N) +
                          ' Sequence1: ' + sequence1_seq[N] +
                          ' Sequence2: ' + sequence2_seq[N] +
                          ' Gene: ' + nucleotide_in_gene_res[0])

    else:
        with open(result_file, 'a') as file_out:
            file_out.write('Comparing ' + sequence1.id + ' and ' + sequence2.id + '\n')
            for N in range(len(sequence1_seq)):
                if sequence1_seq[N] != sequence2_seq[N]:
                    mutations.append(N)
                    if gene_pos is None:
                        file_out.write('Position: ' + str(N) +
                                       ' Sequence1: ' + sequence1_seq[N] +
                                       ' Sequence2: ' + sequence2_seq[N] + '\n')
                    else:
                        nucleotide_in_gene_res = nucleotide_in_gene(N, gene_pos, gene_mutations)
                        gene_mutations = nucleotide_in_gene_res[1]
                        file_out.write('Position: ' + str(N) +
                                       ' Sequence1: ' + sequence1_seq[N] +
                                       ' Sequence2: ' + sequence2_seq[N] +
                                       ' Gene: ' + nucleotide_in_gene_res[0] + '\n')
    return [gene_mutations, mutations]


def insert_mutations_to_df(mutation_table, mutations, sequence1, sequence2):
    for mutation in mutations:
        mutation_table.loc[sequence1[mutation], sequence2[mutation]] += 1
    return mutation_table


def get_mutations_in_msa(multifasta_aligned, file_out=None, gene_orgn=None):
    mutation_table = pd.DataFrame(0, index=['A', 'C', 'G', 'T'], columns=['A', 'C', 'G', 'T'])
    if file_out is not None:
        with open(file_out, 'w') as fo:
            fo.write('')
    if gene_orgn is not None:
        gene_pos = Entrez.genes_in_organism(gene_orgn)
    sequences_list = list(SeqIO.parse(multifasta_aligned, "fasta"))
    gene_mutations = OrderedDict()
    for i in tqdm(range(1, len(sequences_list))):
        sequence1 = sequences_list[-i]
        sequence2 = sequences_list[-i - 1]
        if gene_orgn is None:
            get_mutations(sequence1, sequence2, result_file=file_out)
        else:
            [gene_mutations, mutations] = get_mutations(sequence1, sequence2, result_file=file_out, gene_pos=gene_pos,
                                                        gene_mutations=gene_mutations)
            mutation_table = insert_mutations_to_df(mutation_table, mutations, sequence1, sequence2)
    gene_mutation_report('Virus/gene_mutations.txt', gene_mutations, mutation_proportion_dict=gene_pos)
    return [gene_mutations, mutation_table]


def nucleotide_in_gene(nuc_pos, gene_pos, gene_mutations_dict=None):
    genes = ''
    genes_list = list(gene_pos.keys())
    genes_list.sort()
    for gene in genes_list:
        if int(min(gene_pos[gene])) < nuc_pos < int(max(gene_pos[gene])):
            genes += ',' + gene
            if gene_mutations_dict is not None:
                gene_mutations_dict[gene] = gene_mutations_dict.get(gene, 0) + 1

    if genes == '':
        genes = 'none'
    else:
        genes = genes[1:]
    if gene_mutations_dict is None:
        return [genes]
    else:
        return [genes, gene_mutations_dict]


def gene_mutation_report(filepath_out, gene_mutation_dict, mutation_proportion_dict=None):
    with open(filepath_out, 'w') as fout:
        genes = list(gene_mutation_dict)
        genes.sort()
        if mutation_proportion_dict is None:
            fout.write('gene id (NCBI) : mutation count\n')
            for key in genes:
                fout.write(key + ': ' + str(gene_mutation_dict[key]) + '\n')
        else:
            fout.write('gene id (NCBI) : mutation count - gene mutation index\n')
            for key in genes:
                gene_size = int(max(mutation_proportion_dict[key])) - int(min(mutation_proportion_dict[key]))
                fout.write(key + ': ' + str(gene_mutation_dict[key]) + ' - ' +
                           str(round(int(gene_mutation_dict[key]) / gene_size, 3)) + '\n')


def count_muts(file):
    mutation_table = pd.DataFrame(0, index=['A', 'C', 'G', 'T'], columns=['A', 'C', 'G', 'T'])
    counter = 0
    with open(file, 'r') as fin:
        for line in fin:
            ls = line.split()
            if ls[0] == 'Position:':
                mutation_table.loc[ls[3], ls[5]] += 1
                if ls[3] == 'A' and ls[5] == 'T':
                    counter += 1
    return mutation_table